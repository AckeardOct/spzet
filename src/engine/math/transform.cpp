#include "transform.h"

#include "engine/common/logger.h"
#include "engine/math/utils.h"

void Transform::setRotation(float degX, float degY, float degZ)
{
    orientation = glm::quat(Vec3f(m::rad(degX), m::rad(degY), m::rad(degZ)));
}

void Transform::getModelMatrix(Mat4f& modelOut) const
{
    modelOut = glm::identity<Mat4f>();
    modelOut = glm::translate(modelOut, Vec3f(position.x, position.y, position.z));
    modelOut = modelOut * glm::mat4_cast(orientation);
    modelOut = glm::scale(modelOut, sizeScale);
}

Vec3f Transform::getForward() const
{
    Vec3f ret = Vec3f_UnitZ * orientation;
    ASSERT(m::isNormalized(ret));
    //m::normalize(ret);
    return ret;
}
