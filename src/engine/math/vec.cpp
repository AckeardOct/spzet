#include "vec.h"

#include "engine/common/logger.h"

namespace m {
const float EPSILON = static_cast<float>(1e-5);

float abs(float val)
{
    return std::abs(val);
}

bool isEq(float one, float two)
{
    float diff = m::abs(one - two);
    if (diff > EPSILON) {
        return false;
    }
    return true;
}

bool isZero(float val)
{
    return m::isEq(val, 0.f);
}

bool isZero(const Vec2f& vec)
{
    if (!m::isZero(vec.x)) {
        return false;
    } else if (!m::isZero(vec.y)) {
        return false;
    }
    return true;
}

bool isZero(const Vec3f& vec)
{
    if (!m::isZero(vec.x)) {
        return false;
    } else if (!m::isZero(vec.y)) {
        return false;
    } else if (!m::isZero(vec.z)) {
        return false;
    }
    return true;
}

void normalize(Vec2f& vec)
{
    ASSERT(!m::isZero(vec));
    vec = glm::normalize(vec);
}

void normalize(Vec3f& vec)
{
    ASSERT(!m::isZero(vec));
    vec = glm::normalize(vec);
}

float length(Vec2f& vec)
{
    return glm::length(vec);
}

float length(Vec3f& vec)
{
    return glm::length(vec);
}

bool isNormalized(Vec2f& vec)
{
    return m::isEq(1.f, m::length(vec));
}

bool isNormalized(Vec3f& vec)
{
    return m::isEq(1.f, m::length(vec));
}

}
