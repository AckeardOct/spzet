#pragma once

#include "engine/common/str.h"

using Hash = size_t;

namespace m {
Hash hash(StringRef str);
float rad(float angle);
float deg(float angle);
float cos(float val);
float sin(float val);
}
