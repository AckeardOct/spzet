#include "utils.h"
#include "glm/trigonometric.hpp"

namespace m {

Hash hash(StringRef str)
{
    static std::hash<StringRef> generate;
    return generate(str);
}

float rad(float angle)
{
    return glm::radians(angle);
}

float deg(float angle)
{
    return glm::degrees(angle);
}

float cos(float val)
{
    return glm::cos(val);
}

float sin(float val)
{
    return glm::sin(val);
}

}
