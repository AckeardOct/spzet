#pragma once

#include "engine/math/vec.h"

class Transform {
public:
    void setRotation(float degX, float degY, float degZ);

    void getModelMatrix(Mat4f& model) const;
    Vec3f getForward() const;

    Vec3f sizeScale = Vec3f_Unit;
    Quat orientation = glm::angleAxis(0.f, Vec3f(0, 1, 0));
    Vec3f position = Vec3f_Zero;
};
