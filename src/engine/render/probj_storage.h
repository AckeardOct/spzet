#pragma once

#include "engine/math/utils.h"
#include "engine/render/shader.h"

#include <unordered_map>

struct Probj {
    ShaderRef shader;
    uint vao = 0;
    uint vbo = 0;
    uint verticesSize = 0;
    uint diffuseTexture = 0;
    uint specularTexture = 0;
    bool inited = false;
};

class ProbjStorage {
public:
    ProbjStorage();

    Probj get(StringRef name, StringRef overrideShader = "");

private:
    std::unordered_map<Hash, Probj> storage;
};

extern ProbjStorage probjStorage;
