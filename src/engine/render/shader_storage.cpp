#include "shader_storage.h"

#include "engine/common/logger.h"

ShaderStorage shaderStorage;

ShaderStorage::ShaderStorage()
{
    static int count = 0;
    count++;
    ASSERT_MSG(1 == count, "Second ShaderStorage instance");
}

ShaderRef ShaderStorage::get(StringRef name)
{
    Hash hash = m::hash(name);

    auto it = storage.find(hash);
    if (it != storage.end()) {
        return ShaderRef(it->second);
    }

    LogMsg("Lazy load shader: %s", name.data());
    const String shadersDir = "./shaders/";
    const String shaderDir = shadersDir + name.data() + "/";
    const String vertexShaderFile = shaderDir + name.data() + ".vsh";
    const String fragmentShaderFile = shaderDir + name.data() + ".fsh";

    Shader& retShader = storage.emplace_hint(it, hash, Shader())->second;
    retShader.compile(vertexShaderFile, fragmentShaderFile);
    return ShaderRef(retShader);
}
