#pragma once

#include "engine/common/str.h"
#include "engine/math/utils.h"

#include <unordered_map>

using TextureId = unsigned int;

class TextureStorage {
public:
    TextureStorage();

    TextureId get(StringRef name);

private:
    std::unordered_map<Hash, TextureId> storage;
};

extern TextureStorage textureStorage;
