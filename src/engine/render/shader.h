#pragma once

#include "engine/common/str.h"
#include "engine/math/vec.h"
#include <unordered_map>

class Shader {
public:
    Shader() = default;
    explicit Shader(StringRef vertexShaderFile, StringRef fragmentShaderFile);
    ~Shader();

    bool compile(StringRef vertexShaderFile, StringRef fragmentShaderFile);
    bool compile();

    uint handle() const { return shaderProgram; }

private:
    uint shaderProgram = 0;
    String vertexShaderFile;
    String fragmentShaderFile;
};

struct ShaderRef {
public:
    explicit ShaderRef() = default;
    explicit ShaderRef(const Shader& shader)
        : shaderProgramHandle(shader.handle())
    {
    }

public:
    uint handle() const { return shaderProgramHandle; }
    void setBool(StringRef param, bool val) const;
    void setInt(StringRef param, int val) const;
    void setFloat(StringRef param, float val) const;
    void setVec3f(StringRef param, const Vec3f& vec) const;
    void setVec4f(StringRef param, const Vec4f& vec) const;
    void setMat4f(StringRef param, const Mat4f& mat) const;

private:
    unsigned int shaderProgramHandle = -1;
};
