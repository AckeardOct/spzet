#pragma once

#include "engine/math/vec.h"

using Color4 = Vec4f;
using Color = Vec3f;

const Color Color_Red = Color(1.f, 0.f, 0.f);
const Color Color_Green = Color(0.f, 1.f, 0.f);
const Color Color_Blue = Color(0.f, 0.f, 1.f);
const Color Color_Yellow = Color(1.f, 1.f, 0.f);
const Color Color_White = Color(1.f, 1.f, 1.f);
const Color Color_Grey = Color(0.5f, 0.5f, 0.5f);
const Color Color_Black = Color(0.f, 0.f, 0.f);
