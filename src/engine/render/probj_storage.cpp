#include "probj_storage.h"

#include "engine/common/logger.h"
#include "engine/render/shader_storage.h"
#include "engine/render/texture_storage.h"

#include <GLES3/gl3.h>

static bool CreatePrimCube(Probj& outObj)
{
    // 3 float - vertix
    float vertices[] = {
        -0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        -0.5f, +0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f, -0.5f, +0.5f,
        +0.5f, -0.5f, +0.5f,
        +0.5f, +0.5f, +0.5f,
        +0.5f, +0.5f, +0.5f,
        -0.5f, +0.5f, +0.5f,
        -0.5f, -0.5f, +0.5f,

        -0.5f, +0.5f, +0.5f,
        -0.5f, +0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, +0.5f,
        -0.5f, +0.5f, +0.5f,

        +0.5f, +0.5f, +0.5f,
        +0.5f, +0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, +0.5f,
        +0.5f, +0.5f, +0.5f,

        -0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, +0.5f,
        +0.5f, -0.5f, +0.5f,
        -0.5f, -0.5f, +0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f, +0.5f, -0.5f,
        +0.5f, +0.5f, -0.5f,
        +0.5f, +0.5f, +0.5f,
        +0.5f, +0.5f, +0.5f,
        -0.5f, +0.5f, +0.5f,
        -0.5f, +0.5f, -0.5f
    };
    outObj.verticesSize = sizeof(vertices) / sizeof(vertices[0]);

    glGenVertexArrays(1, &outObj.vao);
    glGenBuffers(1, &outObj.vbo);

    glBindVertexArray(outObj.vao);
    glBindBuffer(GL_ARRAY_BUFFER, outObj.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    outObj.shader = shaderStorage.get("solid");
    outObj.inited = true;
    return true;
}

static bool CreatePrimLine(Probj& outObj)
{
    // 3 float - vertix
    float vertices[] = {
        -0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        -0.5f, +0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f, -0.5f, +0.5f,
        +0.5f, -0.5f, +0.5f,
        +0.5f, +0.5f, +0.5f,
        +0.5f, +0.5f, +0.5f,
        -0.5f, +0.5f, +0.5f,
        -0.5f, -0.5f, +0.5f,

        -0.5f, +0.5f, +0.5f,
        -0.5f, +0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, +0.5f,
        -0.5f, +0.5f, +0.5f,

        +0.5f, +0.5f, +0.5f,
        +0.5f, +0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, +0.5f,
        +0.5f, +0.5f, +0.5f,

        -0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, +0.5f,
        +0.5f, -0.5f, +0.5f,
        -0.5f, -0.5f, +0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f, +0.5f, -0.5f,
        +0.5f, +0.5f, -0.5f,
        +0.5f, +0.5f, +0.5f,
        +0.5f, +0.5f, +0.5f,
        -0.5f, +0.5f, +0.5f,
        -0.5f, +0.5f, -0.5f
    };
    outObj.verticesSize = sizeof(vertices) / sizeof(vertices[0]);

    for (int i = 0; i < outObj.verticesSize; ++i) {
        vertices[i] += 0.5f;
    }

    glGenVertexArrays(1, &outObj.vao);
    glGenBuffers(1, &outObj.vbo);

    glBindVertexArray(outObj.vao);
    glBindBuffer(GL_ARRAY_BUFFER, outObj.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    outObj.shader = shaderStorage.get("solid");
    outObj.inited = true;
    return true;
}

static bool CreatePrimTexture(Probj& outObj)
{
    // 3 float - vertix; 2 float - texture coords
    float vertices[] = {
        -0.5f, -0.5f, -0.5f, 0.0f, 0.0f,
        +0.5f, -0.5f, -0.5f, 1.0f, 0.0f,
        +0.5f, +0.5f, -0.5f, 1.0f, 1.0f,
        +0.5f, +0.5f, -0.5f, 1.0f, 1.0f,
        -0.5f, +0.5f, -0.5f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, 0.0f, 0.0f,

        -0.5f, -0.5f, +0.5f, 0.0f, 0.0f,
        +0.5f, -0.5f, +0.5f, 1.0f, 0.0f,
        +0.5f, +0.5f, +0.5f, 1.0f, 1.0f,
        +0.5f, +0.5f, +0.5f, 1.0f, 1.0f,
        -0.5f, +0.5f, +0.5f, 0.0f, 1.0f,
        -0.5f, -0.5f, +0.5f, 0.0f, 0.0f,

        -0.5f, +0.5f, +0.5f, 1.0f, 0.0f,
        -0.5f, +0.5f, -0.5f, 1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
        -0.5f, -0.5f, +0.5f, 0.0f, 0.0f,
        -0.5f, +0.5f, +0.5f, 1.0f, 0.0f,

        +0.5f, +0.5f, +0.5f, 1.0f, 0.0f,
        +0.5f, +0.5f, -0.5f, 1.0f, 1.0f,
        +0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
        +0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
        +0.5f, -0.5f, +0.5f, 0.0f, 0.0f,
        +0.5f, +0.5f, +0.5f, 1.0f, 0.0f,

        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f,
        +0.5f, -0.5f, -0.5f, 1.0f, 1.0f,
        +0.5f, -0.5f, +0.5f, 1.0f, 0.0f,
        +0.5f, -0.5f, +0.5f, 1.0f, 0.0f,
        -0.5f, -0.5f, +0.5f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f,

        -0.5f, +0.5f, -0.5f, 0.0f, 1.0f,
        +0.5f, +0.5f, -0.5f, 1.0f, 1.0f,
        +0.5f, +0.5f, +0.5f, 1.0f, 0.0f,
        +0.5f, +0.5f, +0.5f, 1.0f, 0.0f,
        -0.5f, +0.5f, +0.5f, 0.0f, 0.0f,
        -0.5f, +0.5f, -0.5f, 0.0f, 1.0f
    };
    outObj.verticesSize = 36;

    glGenVertexArrays(1, &outObj.vao);
    glGenBuffers(1, &outObj.vbo);

    glBindVertexArray(outObj.vao);
    glBindBuffer(GL_ARRAY_BUFFER, outObj.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // clear
    glEnableVertexAttribArray(GL_NONE);
    glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
    glBindVertexArray(GL_NONE);

    outObj.diffuseTexture = textureStorage.get("container.jpg");
    outObj.shader = shaderStorage.get("texture");
    outObj.inited = true;
    return true;
}

static bool CreateLightTexture(Probj& outObj)
{
    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    float vertices[] = {
        // 3f positions       // 3f normals       // 2f texture coords
        -0.5f, -0.5f, -0.5f, +0.0f, +0.0f, -1.0f, +0.0f, +0.0f,
        +0.5f, -0.5f, -0.5f, +0.0f, +0.0f, -1.0f, +1.0f, +0.0f,
        +0.5f, +0.5f, -0.5f, +0.0f, +0.0f, -1.0f, +1.0f, +1.0f,
        +0.5f, +0.5f, -0.5f, +0.0f, +0.0f, -1.0f, +1.0f, +1.0f,
        -0.5f, +0.5f, -0.5f, +0.0f, +0.0f, -1.0f, +0.0f, +1.0f,
        -0.5f, -0.5f, -0.5f, +0.0f, +0.0f, -1.0f, +0.0f, +0.0f,

        -0.5f, -0.5f, +0.5f, +0.0f, +0.0f, +1.0f, +0.0f, +0.0f,
        +0.5f, -0.5f, +0.5f, +0.0f, +0.0f, +1.0f, +1.0f, +0.0f,
        +0.5f, +0.5f, +0.5f, +0.0f, +0.0f, +1.0f, +1.0f, +1.0f,
        +0.5f, +0.5f, +0.5f, +0.0f, +0.0f, +1.0f, +1.0f, +1.0f,
        -0.5f, +0.5f, +0.5f, +0.0f, +0.0f, +1.0f, +0.0f, +1.0f,
        -0.5f, -0.5f, +0.5f, +0.0f, +0.0f, +1.0f, +0.0f, +0.0f,

        -0.5f, +0.5f, +0.5f, -1.0f, +0.0f, +0.0f, +1.0f, +0.0f,
        -0.5f, +0.5f, -0.5f, -1.0f, +0.0f, +0.0f, +1.0f, +1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f, +0.0f, +0.0f, +0.0f, +1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f, +0.0f, +0.0f, +0.0f, +1.0f,
        -0.5f, -0.5f, +0.5f, -1.0f, +0.0f, +0.0f, +0.0f, +0.0f,
        -0.5f, +0.5f, +0.5f, -1.0f, +0.0f, +0.0f, +1.0f, +0.0f,

        +0.5f, +0.5f, +0.5f, +1.0f, +0.0f, +0.0f, +1.0f, +0.0f,
        +0.5f, +0.5f, -0.5f, +1.0f, +0.0f, +0.0f, +1.0f, +1.0f,
        +0.5f, -0.5f, -0.5f, +1.0f, +0.0f, +0.0f, +0.0f, +1.0f,
        +0.5f, -0.5f, -0.5f, +1.0f, +0.0f, +0.0f, +0.0f, +1.0f,
        +0.5f, -0.5f, +0.5f, +1.0f, +0.0f, +0.0f, +0.0f, +0.0f,
        +0.5f, +0.5f, +0.5f, +1.0f, +0.0f, +0.0f, +1.0f, +0.0f,

        -0.5f, -0.5f, -0.5f, +0.0f, -1.0f, +0.0f, +0.0f, +1.0f,
        +0.5f, -0.5f, -0.5f, +0.0f, -1.0f, +0.0f, +1.0f, +1.0f,
        +0.5f, -0.5f, +0.5f, +0.0f, -1.0f, +0.0f, +1.0f, +0.0f,
        +0.5f, -0.5f, +0.5f, +0.0f, -1.0f, +0.0f, +1.0f, +0.0f,
        -0.5f, -0.5f, +0.5f, +0.0f, -1.0f, +0.0f, +0.0f, +0.0f,
        -0.5f, -0.5f, -0.5f, +0.0f, -1.0f, +0.0f, +0.0f, +1.0f,

        -0.5f, +0.5f, -0.5f, +0.0f, +1.0f, +0.0f, +0.0f, +1.0f,
        +0.5f, +0.5f, -0.5f, +0.0f, +1.0f, +0.0f, +1.0f, +1.0f,
        +0.5f, +0.5f, +0.5f, +0.0f, +1.0f, +0.0f, +1.0f, +0.0f,
        +0.5f, +0.5f, +0.5f, +0.0f, +1.0f, +0.0f, +1.0f, +0.0f,
        -0.5f, +0.5f, +0.5f, +0.0f, +1.0f, +0.0f, +0.0f, +0.0f,
        -0.5f, +0.5f, -0.5f, +0.0f, +1.0f, +0.0f, +0.0f, +1.0f
    };
    outObj.verticesSize = 36;

    glGenVertexArrays(1, &outObj.vao);
    glGenBuffers(1, &outObj.vbo);

    glBindVertexArray(outObj.vao);
    glBindBuffer(GL_ARRAY_BUFFER, outObj.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // vertex
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // normal
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    // texture
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);

    // clear
    glEnableVertexAttribArray(GL_NONE);
    glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
    glBindVertexArray(GL_NONE);

    outObj.diffuseTexture = textureStorage.get("container2.png");
    outObj.specularTexture = textureStorage.get("container2_specular.png");
    outObj.shader = shaderStorage.get("l_texture");
    outObj.inited = true;
    return true;
}

static bool CreateLightMaterial(Probj& outObj)
{
    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    float vertices[] = {
        // 3f positions       // 3f normals
        -0.5f, -0.5f, -0.5f, +0.0f, +0.0f, -1.0f,
        +0.5f, -0.5f, -0.5f, +0.0f, +0.0f, -1.0f,
        +0.5f, +0.5f, -0.5f, +0.0f, +0.0f, -1.0f,
        +0.5f, +0.5f, -0.5f, +0.0f, +0.0f, -1.0f,
        -0.5f, +0.5f, -0.5f, +0.0f, +0.0f, -1.0f,
        -0.5f, -0.5f, -0.5f, +0.0f, +0.0f, -1.0f,

        -0.5f, -0.5f, +0.5f, +0.0f, +0.0f, +1.0f,
        +0.5f, -0.5f, +0.5f, +0.0f, +0.0f, +1.0f,
        +0.5f, +0.5f, +0.5f, +0.0f, +0.0f, +1.0f,
        +0.5f, +0.5f, +0.5f, +0.0f, +0.0f, +1.0f,
        -0.5f, +0.5f, +0.5f, +0.0f, +0.0f, +1.0f,
        -0.5f, -0.5f, +0.5f, +0.0f, +0.0f, +1.0f,

        -0.5f, +0.5f, +0.5f, -1.0f, +0.0f, +0.0f,
        -0.5f, +0.5f, -0.5f, -1.0f, +0.0f, +0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f, +0.0f, +0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f, +0.0f, +0.0f,
        -0.5f, -0.5f, +0.5f, -1.0f, +0.0f, +0.0f,
        -0.5f, +0.5f, +0.5f, -1.0f, +0.0f, +0.0f,

        +0.5f, +0.5f, +0.5f, +1.0f, +0.0f, +0.0f,
        +0.5f, +0.5f, -0.5f, +1.0f, +0.0f, +0.0f,
        +0.5f, -0.5f, -0.5f, +1.0f, +0.0f, +0.0f,
        +0.5f, -0.5f, -0.5f, +1.0f, +0.0f, +0.0f,
        +0.5f, -0.5f, +0.5f, +1.0f, +0.0f, +0.0f,
        +0.5f, +0.5f, +0.5f, +1.0f, +0.0f, +0.0f,

        -0.5f, -0.5f, -0.5f, +0.0f, -1.0f, +0.0f,
        +0.5f, -0.5f, -0.5f, +0.0f, -1.0f, +0.0f,
        +0.5f, -0.5f, +0.5f, +0.0f, -1.0f, +0.0f,
        +0.5f, -0.5f, +0.5f, +0.0f, -1.0f, +0.0f,
        -0.5f, -0.5f, +0.5f, +0.0f, -1.0f, +0.0f,
        -0.5f, -0.5f, -0.5f, +0.0f, -1.0f, +0.0f,

        -0.5f, +0.5f, -0.5f, +0.0f, +1.0f, +0.0f,
        +0.5f, +0.5f, -0.5f, +0.0f, +1.0f, +0.0f,
        +0.5f, +0.5f, +0.5f, +0.0f, +1.0f, +0.0f,
        +0.5f, +0.5f, +0.5f, +0.0f, +1.0f, +0.0f,
        -0.5f, +0.5f, +0.5f, +0.0f, +1.0f, +0.0f,
        -0.5f, +0.5f, -0.5f, +0.0f, +1.0f, +0.0f
    };
    outObj.verticesSize = 36;

    glGenVertexArrays(1, &outObj.vao);
    glGenBuffers(1, &outObj.vbo);

    glBindVertexArray(outObj.vao);
    glBindBuffer(GL_ARRAY_BUFFER, outObj.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // vertex
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // normal
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // clear
    glEnableVertexAttribArray(GL_NONE);
    glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
    glBindVertexArray(GL_NONE);

    outObj.shader = shaderStorage.get("l_material");
    outObj.inited = true;
    return true;
}

ProbjStorage probjStorage;

ProbjStorage::ProbjStorage()
{
    static int count = 0;
    count++;
    ASSERT_MSG(1 == count, "Second ProbjStorage instance");
}

Probj ProbjStorage::get(StringRef name, StringRef overrideShader /*= ""*/)
{
    Hash hash = m::hash(String(name.data()) + String(overrideShader.data()));

    auto it = storage.find(hash);
    if (it != storage.end()) {
        return it->second;
    }

    LogMsg("Lazy load probj: %s, overrideShader: %s", name.data(), overrideShader.data());
    auto& retProObj = storage.emplace_hint(it, hash, Probj())->second;
    if ("pro_cube" == name) {
        CreatePrimCube(retProObj);
    } else if ("pro_t_cube" == name) {
        CreatePrimTexture(retProObj);
    } else if ("pro_line" == name) {
        CreatePrimLine(retProObj);
    } else if ("pro_lt_container" == name) {
        CreateLightTexture(retProObj);
    } else if ("pro_lm_container" == name) {
        CreateLightMaterial(retProObj);
    } else {
        ASSERT_FAIL("unknown probj");
    }

    if (!overrideShader.empty()) {
        retProObj.shader = shaderStorage.get(overrideShader);
    }

    return retProObj;
}
