#include "shader.h"

#include <GLES3/gl3.h>
#include <glm/gtc/type_ptr.hpp>

#include "engine/common/file_system.h"
#include "engine/common/logger.h"

static GLuint loadShader(GLenum shaderType, const char* source)
{
    ASSERT(GL_VERTEX_SHADER == shaderType || GL_FRAGMENT_SHADER == shaderType);

    GLuint retShader = 0;
    retShader = glCreateShader(shaderType);
    if (0 == retShader) {
        ASSERT_FAIL("Can't create shader");
        return 0;
    }

    glShaderSource(retShader, 1, &source, NULL);
    glCompileShader(retShader);

    GLint compiledStatus = 0;
    glGetShaderiv(retShader, GL_COMPILE_STATUS, &compiledStatus);

    if (0 == compiledStatus) {
        GLint infoLen = 0;
        glGetShaderiv(retShader, GL_INFO_LOG_LENGTH, &infoLen);
        ASSERT(infoLen > 0);
        String info;
        info.resize(static_cast<size_t>(infoLen));
        glGetShaderInfoLog(retShader, infoLen, NULL, info.data());
        ASSERT_FAIL("Can't compile shader. error: %s", info.c_str());
        glDeleteShader(retShader);
        return 0;
    }
    return retShader;
}

Shader::Shader(StringRef vertexShaderFile, StringRef fragmentShaderFile)
    : vertexShaderFile(vertexShaderFile)
    , fragmentShaderFile(fragmentShaderFile)
{
}

Shader::~Shader()
{
    if (shaderProgram != 0) {
        LogMsg("Remove shader: (%s %s)", vertexShaderFile.c_str(), fragmentShaderFile.c_str());
        glDeleteProgram(shaderProgram);
    }
}

bool Shader::compile(StringRef vertexShaderFile, StringRef fragmentShaderFile)
{
    this->vertexShaderFile = vertexShaderFile;
    this->fragmentShaderFile = fragmentShaderFile;
    return compile();
}

bool Shader::compile()
{
    GLuint vertexShader = 0;
    String vertexSrc = fs::cat(vertexShaderFile);
    LogMsg("Try compile shader: %s", vertexShaderFile.data());
    vertexShader = loadShader(GL_VERTEX_SHADER, vertexSrc.data());

    GLuint fragmentShader = 0;
    String fragmentSrc = fs::cat(fragmentShaderFile);
    LogMsg("Try compile shader: %s", fragmentShaderFile.data());
    fragmentShader = loadShader(GL_FRAGMENT_SHADER, fragmentSrc.data());

    shaderProgram = glCreateProgram();
    if (0 == shaderProgram) {
        ASSERT_FAIL("Can't create shader program (%s, %s)", vertexShaderFile.data(), fragmentShaderFile.data());
        return false;
    }
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    GLint linkStatus = 0;
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &linkStatus);
    if (0 == linkStatus) {
        GLint infoLen = 0;
        glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &infoLen);
        ASSERT(infoLen > 0);
        String info;
        info.resize(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(shaderProgram, infoLen, NULL, info.data());
        ASSERT_FAIL("Can't link shader program. error: %s", info.c_str());
        glDeleteProgram(shaderProgram);
        return false;
    }

    return true;
}

static GLint GetUniformLocation(StringRef param, GLuint shaderProgram)
{
    GLint location = glGetUniformLocation(shaderProgram, param.data());

    // check error
    switch (location) {
    case static_cast<GLint>(GL_INVALID_INDEX):
        ASSERT_FAIL("Can't get location of uniform: %s. error: GL_INVALID_INDEX.", param.data());
        break;
    case GL_INVALID_VALUE:
        ASSERT_FAIL("Can't get location of uniform: %s. error: GL_INVALID_VALUE.", param.data());
        break;
    case GL_INVALID_OPERATION:
        ASSERT_FAIL("Can't get location of uniform: %s. error: GL_INVALID_OPERATION.", param.data());
        break;
    }

    return location;
}

void ShaderRef::setBool(StringRef param, bool val) const
{
    GLint location = GetUniformLocation(param, handle());
    int i = (val ? 1 : 0);
    glUniform1i(location, i);
}

void ShaderRef::setInt(StringRef param, int val) const
{
    GLint location = GetUniformLocation(param, handle());
    glUniform1i(location, val);
}

void ShaderRef::setFloat(StringRef param, float val) const
{
    GLint location = GetUniformLocation(param, handle());
    glUniform1f(location, val);
}

void ShaderRef::setVec3f(StringRef param, const Vec3f& vec) const
{
    GLint location = GetUniformLocation(param, handle());
    glUniform3f(location, vec.x, vec.y, vec.z);
}

void ShaderRef::setVec4f(StringRef param, const Vec4f& vec) const
{
    GLint location = GetUniformLocation(param, handle());
    glUniform4f(location, vec.x, vec.y, vec.z, vec.w);
}

void ShaderRef::setMat4f(StringRef param, const Mat4f& mat) const
{
    GLint location = GetUniformLocation(param, handle());
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(mat));
}
