project(render)

GET_PROPERTY(CONAN_LIBS GLOBAL PROPERTY CONAN_LIBS)

set(src
    color.cpp
    shader.cpp
    shader_storage.cpp
    texture_storage.cpp
    probj_storage.cpp
)

add_library(${PROJECT_NAME} STATIC ${src})
add_library(engine::${PROJECT_NAME} ALIAS ${PROJECT_NAME})
target_link_libraries(${PROJECT_NAME} ${CONAN_LIBS} engine::common)
