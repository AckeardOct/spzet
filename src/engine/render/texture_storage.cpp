#include "texture_storage.h"

#include "engine/common/logger.h"

#include <GLES3/gl3.h>
#include <SDL2/SDL_image.h>

struct TextureRaw {

    TextureRaw(StringRef texturePath)
    {
        surface = IMG_Load(texturePath.data());
        ASSERT_MSG(surface, "Can't IMG_Load(%s). Error: %s", texturePath.data(), IMG_GetError());
    }

    ~TextureRaw()
    {
        SDL_FreeSurface(surface);
    }

    int w() { return surface->w; }
    int h() { return surface->h; }
    void* data() { return surface->pixels; }

    int mode()
    {
        if (4 == surface->format->BytesPerPixel) {
            return GL_RGBA;
        }
        ASSERT(3 == surface->format->BytesPerPixel);
        return GL_RGB;
    }

private:
    SDL_Surface* surface = nullptr;
};

TextureStorage textureStorage;

TextureStorage::TextureStorage()
{
    static int count = 0;
    count++;
    ASSERT_MSG(1 == count, "Second TextureStorage instance");
}

TextureId TextureStorage::get(StringRef name)
{
    Hash hash = m::hash(name);

    auto it = storage.find(hash);
    if (it != storage.end()) {
        return it->second;
    }

    LogMsg("Lazy load texture: %s", name.data());
    const String texturesDir = "./textures/";
    const String textureFile = texturesDir + name.data();

    TextureId texture = 0;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    TextureRaw rawTex(textureFile);
    glTexImage2D(GL_TEXTURE_2D, 0, rawTex.mode(), rawTex.w(), rawTex.h(), 0, rawTex.mode(), GL_UNSIGNED_BYTE, rawTex.data());
    glGenerateMipmap(GL_TEXTURE_2D);

    storage[hash] = texture;
    return texture;
}
