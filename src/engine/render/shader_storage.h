#pragma once

#include "engine/common/str.h"
#include "engine/math/utils.h"
#include "engine/render/shader.h"

#include <unordered_map>

class ShaderStorage {
public:
    ShaderStorage();
    ShaderRef get(StringRef name);

private:
    std::unordered_map<Hash, Shader> storage;
};

extern ShaderStorage shaderStorage;
