#include "sys_logic.h"

#include "engine/math/vec.h"
#include <entt/entity/registry.hpp>

#include "engine/common/camera_3d.h"

static void CameraInput(const Vec3f& direction, const Vec2f& mouseShift, entt::registry& reg, float dt)
{
    auto view = reg.view<cl::Trf, cl::CameraInput>();
    for (auto ent : view) {
        auto& trfCmp = view.get<cl::Trf>(ent);
        auto& camCmp = view.get<cl::CameraInput>(ent);

        // MEGA HACK
        Camera3D* cam3d = (Camera3D*)camCmp.camera;
        Vec3f move = dt * direction * camCmp.speed / 1'000.f;
        cam3d->dbgProcessMovement(move);
        cam3d->dbgProcessMouse(mouseShift);
    }
}

InputSys::InputSys()
{
    keyboardKeys.reserve(SDL_NUM_SCANCODES);
    for (auto& iter : keyboardKeys) {
        iter.second = false;
    }

    mouseKeys.reserve(5);
    for (auto& iter : mouseKeys) {
        iter.second = false;
    }
}

void InputSys::onInput(const SDL_Event& event)
{
    events.push_back(event);
    switch (event.type) {
    case SDL_KEYDOWN:
        keyboardKeys[event.key.keysym.sym] = true;
        break;
    case SDL_KEYUP:
        keyboardKeys[event.key.keysym.sym] = false;
        break;
    case SDL_MOUSEBUTTONDOWN:
        mouseKeys[event.button.button] = true;
        break;
    case SDL_MOUSEBUTTONUP:
        mouseKeys[event.button.button] = false;
        break;
    case SDL_MOUSEMOTION:
        Vec2f newMousePos(event.motion.x, event.motion.y);
        Vec2f shift = newMousePos - mousePos;
        if (mouseKeys[SDL_BUTTON_RIGHT] && firstMouseEvent == false) {
            mouseShift += shift;
        } else {
            firstMouseEvent = false;
        }
        mousePos = newMousePos;
        break;
    }
}

void InputSys::update(entt::registry& reg, float dt)
{
    const float mouseKeyShift = 4.f;
    Vec3f direction = Vec3f_Zero;
    for (auto& iter : keyboardKeys) {
        if (iter.second) {
            switch (iter.first) {
            case SDLK_w:
                direction.z += 1.f;
                break;
            case SDLK_s:
                direction.z -= 1.f;
                break;
            case SDLK_a:
                direction.x -= 1.f;
                break;
            case SDLK_d:
                direction.x += 1.f;
                break;
            case SDLK_e:
                direction.y += 1.f;
                break;
            case SDLK_q:
                direction.y -= 1.f;
                break;

            case SDLK_UP: // mouse shift
                mouseShift.y -= mouseKeyShift;
                break;
            case SDLK_DOWN:
                mouseShift.y += mouseKeyShift;
                break;
            case SDLK_LEFT:
                mouseShift.x -= mouseKeyShift;
                break;
            case SDLK_RIGHT:
                mouseShift.x += mouseKeyShift;
                break;
            }
        }
    }

    if (!m::isZero(direction)) {
        m::normalize(direction);
    }
    CameraInput(direction, mouseShift, reg, dt);

    events.clear(); // TODO: move to SCOPE_EXIT
    mouseShift = Vec2f_Zero;
}
