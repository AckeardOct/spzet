#include "scene_cube.h"

#include "engine/common/camera_3d.h"
#include "engine/common/comp_logic.h"
#include "engine/common/comp_render.h"
#include "engine/common/logger.h"
#include "engine/common/sys_logic.h"
#include "engine/common/sys_render.h"

CubeScene::CubeScene(GameWindow& window)
    : IScene(window)
{
    camera = new Camera3D(window, Vec3f(0.f, 1.5f, 5.f));

    initLogicSystems();
    initRenderSystems();
    initEntities();
}

CubeScene::~CubeScene()
{
    for (ILogicSys* sys : logicSystems) {
        delete sys;
    }
    logicSystems.clear();

    for (IRenderSys* sys : renderSystems) {
        delete sys;
    }
    renderSystems.clear();

    ASSERT(camera);
    delete camera;
    camera = nullptr;
}

void CubeScene::input(float dt, const SDL_Event& event)
{
    if (inputSys) {
        inputSys->onInput(event);
    }
}

void CubeScene::update(float dt)
{
    for (ILogicSys* sys : logicSystems) {
        sys->update(reg, dt);
    }
}

void CubeScene::render(float dt)
{
    for (IRenderSys* sys : renderSystems) {
        sys->update(reg, *camera);
    }
}

void CubeScene::initLogicSystems()
{
    inputSys = new InputSys();
    logicSystems.push_back(inputSys);
    logicSystems.push_back(new MoveSys());
}

void CubeScene::initRenderSystems()
{
    renderSystems.push_back(new CubeRenderSys());
}

#include <glm/gtc/quaternion.hpp>

void CubeScene::initEntities()
{
    { // camera
        auto ent = reg.create();
        reg.assign<cl::Trf>(ent);
        reg.assign<cl::CameraInput>(ent, camera, 2.f);
    }

    { // direct light
        auto ent = reg.create();
        auto& trf = reg.assign<cl::Trf>(ent);
        trf.transform.position = Vec3f(2, 5, -20);
        trf.transform.setRotation(45.f, 180.f, 0);
        cr::DirectLight::Setup setup;
        {
            setup.ambient = Color(0.05f, 0.05f, 0.05f);
            setup.diffuse = Color(0.4f, 0.4f, 0.4f);
            setup.specular = Color(0.5f, 0.5f, 0.5f);
        }
        reg.assign<cr::DirectLight>(ent, "pro_cube", setup);
    }

    { // point light
        auto ent = reg.create();
        auto& trf = reg.assign<cl::Trf>(ent);
        trf.transform.position = Vec3f(1, 2, -1);
        trf.transform.sizeScale = Vec3f(0.2);
        cr::PointLight::Setup setup;
        {
            setup.ambient = Color_White;
            setup.diffuse = Color_Red;
            setup.specular = Color_Red;
            setup.constant = 1.0f;
            setup.linear = 0.09f;
            setup.quadratic = 0.032f;
        }
        reg.assign<cr::PointLight>(ent, "pro_cube", setup);
    }

    { // spot light
        auto ent = reg.create();
        auto& trf = reg.assign<cl::Trf>(ent);
        trf.transform.sizeScale = Vec3f(0.2f, 0.2f, 0.8f);
        trf.transform.position.y = 1.f;
        trf.transform.position.x = -0.7f;
        trf.transform.setRotation(0.f, -90.f, 0.f);
        cr::SpotLight::Setup setup;
        {
            setup.ambient = Color_Green;
            setup.diffuse = Color_Green;
            setup.specular = Color_Green;
            setup.constant = 1.0f;
            setup.linear = 0.09f;
            setup.quadratic = 0.032f;
            setup.cutOff = m::cos(m::rad(12.5f));
            setup.outerCutOff = m::cos(m::rad(15.f));
        }
        reg.assign<cr::SpotLight>(ent, "pro_cube", setup);
    }

    { // light texture cube
        auto ent = reg.create();
        auto& trf = reg.assign<cl::Trf>(ent);
        trf.transform.sizeScale.x = 0.5;
        trf.transform.position = Vec3f(0, 1, 0);
        reg.assign<cr::LightTexture>(ent, "pro_lt_container", 32.0f);
    }

    { // light material cube
        auto ent = reg.create();
        auto& trf = reg.assign<cl::Trf>(ent);
        trf.transform.position = Vec3f(1, 1, 0);
        cr::MaterialTexture::Setup setup;
        {
            // http://devernay.free.fr/cours/opengl/materials.html
            // emerald	0.0215	0.1745	0.0215	0.07568	0.61424	0.07568	0.633	0.727811	0.633	0.6
            setup.ambient = Vec3f(0.0215, 0.1745, 0.0215);
            setup.diffuse = Vec3f(0.07568, 0.61424, 0.07568);
            setup.specular = Vec3f(0.633, 0.727811, 0.633);
            setup.shiness = 0.6f;
        }
        reg.assign<cr::MaterialTexture>(ent, "pro_lm_container", setup);
    }

    { // pivot
        const float pivotLength = 5.f;
        { // AXIS X
            auto ent = reg.create();
            auto& trf = reg.assign<cl::Trf>(ent);
            trf.transform.sizeScale = Vec3f(pivotLength, 0.01f, 0.01f);
            reg.assign<cr::PrimSolid>(ent, "pro_line", Color_Red);
        }
        { // AXIS Y
            auto ent = reg.create();
            auto& trf = reg.assign<cl::Trf>(ent);
            trf.transform.sizeScale = Vec3f(0.01f, pivotLength, 0.01f);
            reg.assign<cr::PrimSolid>(ent, "pro_line", Color_Green);
        }
        { // AXIS Z
            auto ent = reg.create();
            auto& trf = reg.assign<cl::Trf>(ent);
            trf.transform.sizeScale = Vec3f(0.01f, 0.01f, pivotLength);
            reg.assign<cr::PrimSolid>(ent, "pro_line", Color_Blue);
        }
    }
}
