#pragma once

#include "engine/common/i_system.h"

class CubeRenderSys : public IRenderSys {
public: // IRenderSys interface
    virtual void update(entt::registry& reg, const ICamera& camera) override;
};
