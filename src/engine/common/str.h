#pragma once

#include <memory>
#include <string>
#include <string_view>

using String = std::string;
using StringRef = std::string_view;

template <typename... Args>
String Sprintf(StringRef format, Args... args)
{
    size_t size = snprintf(nullptr, 0, format.data(), args...) + 1; // Extra space for '\0'
    if (size <= 0) {
        throw std::runtime_error("Error during formatting.");
    }
    std::unique_ptr<char[]> buf(new char[size]);
    snprintf(buf.get(), size, format.data(), args...);
    return String(buf.get(), buf.get() + size - 1); // We don't want the '\0' inside
}
