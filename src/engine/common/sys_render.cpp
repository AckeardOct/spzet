#include "sys_render.h"

#include "engine/common/comp_logic.h"
#include "engine/common/comp_render.h"
#include "engine/common/logger.h"

#include <GLES3/gl3.h>
#include <entt/entity/registry.hpp>

#define DEBUG_RENDER

static void renderPrimSolid(entt::registry& reg, const Mat4f& cameraView, const Mat4f& cameraProjection)
{
    auto regView = reg.view<cl::Trf, cr::PrimSolid>();
    for (auto ent : regView) {
        const auto& rndCmp = regView.get<cr::PrimSolid>(ent);
        const auto& trfCmp = regView.get<cl::Trf>(ent);

        ASSERT_MSG(rndCmp.obj.inited, "invalid primitive render object");

        glUseProgram(rndCmp.obj.shader.handle());
        rndCmp.obj.shader.setMat4f("projection", cameraProjection);
        rndCmp.obj.shader.setMat4f("view", cameraView);

        Mat4f model = glm::identity<Mat4f>();
        trfCmp.transform.getModelMatrix(model);
        rndCmp.obj.shader.setMat4f("model", model);
        rndCmp.obj.shader.setVec3f("color", rndCmp.color);

        glBindVertexArray(rndCmp.obj.vao);

        glDrawArrays(GL_TRIANGLES, 0, rndCmp.obj.verticesSize);
    }
}

static void renderPrimTexture(entt::registry& reg, const Mat4f& cameraView, const Mat4f& cameraProjection)
{
    auto regView = reg.view<cl::Trf, cr::PrimTexture>();
    for (auto ent : regView) {
        const auto& rndCmp = regView.get<cr::PrimTexture>(ent);
        const auto& trfCmp = regView.get<cl::Trf>(ent);

        ASSERT_MSG(rndCmp.obj.inited, "invalid primitive render object");

        glUseProgram(rndCmp.obj.shader.handle());
        rndCmp.obj.shader.setMat4f("projection", cameraProjection);
        rndCmp.obj.shader.setMat4f("view", cameraView);

        Mat4f model = glm::identity<Mat4f>();
        trfCmp.transform.getModelMatrix(model);
        rndCmp.obj.shader.setMat4f("model", model);

        glBindVertexArray(rndCmp.obj.vao);
        glBindTexture(GL_TEXTURE_2D, rndCmp.obj.diffuseTexture);

        glDrawArrays(GL_TRIANGLES, 0, rndCmp.obj.verticesSize);
    }
}

static void renderDirectLight(entt::registry& reg, const Mat4f& cameraView, const Mat4f& cameraProjection)
{
    auto regView = reg.view<cl::Trf, cr::DirectLight>();
    for (auto ent : regView) {
        const auto& rndCmp = regView.get<cr::DirectLight>(ent);
        const auto& trfCmp = regView.get<cl::Trf>(ent);

        ASSERT_MSG(rndCmp.obj.inited, "invalid primitive render object");

        glUseProgram(rndCmp.obj.shader.handle());
        rndCmp.obj.shader.setMat4f("projection", cameraProjection);
        rndCmp.obj.shader.setMat4f("view", cameraView);

        Mat4f model = glm::identity<Mat4f>();
        trfCmp.transform.getModelMatrix(model);
        rndCmp.obj.shader.setMat4f("model", model);
        rndCmp.obj.shader.setVec3f("diffuseColor", rndCmp.setup.diffuse);

        glBindVertexArray(rndCmp.obj.vao);

        glDrawArrays(GL_TRIANGLES, 0, rndCmp.obj.verticesSize);
    }
}

static void renderPointLight(entt::registry& reg, const Mat4f& cameraView, const Mat4f& cameraProjection)
{
    auto regView = reg.view<cl::Trf, cr::PointLight>();
    for (auto ent : regView) {
        const auto& rndCmp = regView.get<cr::PointLight>(ent);
        const auto& trfCmp = regView.get<cl::Trf>(ent);

        ASSERT_MSG(rndCmp.obj.inited, "invalid primitive render object");

        glUseProgram(rndCmp.obj.shader.handle());
        rndCmp.obj.shader.setMat4f("projection", cameraProjection);
        rndCmp.obj.shader.setMat4f("view", cameraView);

        Mat4f model = glm::identity<Mat4f>();
        trfCmp.transform.getModelMatrix(model);
        rndCmp.obj.shader.setMat4f("model", model);
        rndCmp.obj.shader.setVec3f("diffuseColor", rndCmp.setup.diffuse);

        glBindVertexArray(rndCmp.obj.vao);

        glDrawArrays(GL_TRIANGLES, 0, rndCmp.obj.verticesSize);
    }
}

static void renderSpotLight(entt::registry& reg, const Mat4f& cameraView, const Mat4f& cameraProjection)
{
    auto regView = reg.view<cl::Trf, cr::SpotLight>();
    for (auto ent : regView) {
        const auto& rndCmp = regView.get<cr::SpotLight>(ent);
        const auto& trfCmp = regView.get<cl::Trf>(ent);

        ASSERT_MSG(rndCmp.obj.inited, "invalid primitive render object");

        glUseProgram(rndCmp.obj.shader.handle());
        rndCmp.obj.shader.setMat4f("projection", cameraProjection);
        rndCmp.obj.shader.setMat4f("view", cameraView);

        Mat4f model = glm::identity<Mat4f>();
        trfCmp.transform.getModelMatrix(model);
        rndCmp.obj.shader.setMat4f("model", model);
        rndCmp.obj.shader.setVec3f("diffuseColor", rndCmp.setup.diffuse);

        glBindVertexArray(rndCmp.obj.vao);

        glDrawArrays(GL_TRIANGLES, 0, rndCmp.obj.verticesSize);
    }
}

// wrong work
static void renderLightMaterial(entt::registry& reg, const ICamera& camera, const Mat4f& cameraView, const Mat4f& cameraProjection)
{
    auto regView = reg.view<cl::Trf, cr::MaterialTexture>();
    for (auto ent : regView) {
        const auto& rndCmp = regView.get<cr::MaterialTexture>(ent);
        const auto& trfCmp = regView.get<cl::Trf>(ent);
        const auto& shader = rndCmp.obj.shader;

        ASSERT_MSG(rndCmp.obj.inited, "invalid primitive render object");

        glUseProgram(shader.handle());

        shader.setVec3f("material.ambient", rndCmp.setup.ambient);
        shader.setVec3f("material.diffuse", rndCmp.setup.diffuse);
        shader.setVec3f("material.specular", rndCmp.setup.specular);
        shader.setFloat("material.shininess", rndCmp.setup.shiness);

        // directional light
        bool enableDirectLight = true;
        shader.setBool("enableDirectLight", enableDirectLight);
        if (enableDirectLight) {
            const int maxDirectLightShaderSupport = 1;
            int iter = 0;
            auto poinLightView = reg.view<cl::Trf, cr::DirectLight>();
            for (auto ent : poinLightView) {
                if (iter >= maxDirectLightShaderSupport) {
                    break;
                }

                const auto& directLightRndCmp = poinLightView.get<cr::DirectLight>(ent);
                const auto& directLightTrfCmp = poinLightView.get<cl::Trf>(ent);

                shader.setVec3f(Sprintf("dirLight[%d].direction", iter), directLightTrfCmp.transform.getForward());
                shader.setVec3f(Sprintf("dirLight[%d].ambient", iter), directLightRndCmp.setup.ambient);
                shader.setVec3f(Sprintf("dirLight[%d].diffuse", iter), directLightRndCmp.setup.diffuse);
                shader.setVec3f(Sprintf("dirLight[%d].specular", iter), directLightRndCmp.setup.specular);
                iter++;
            }
        }

        // point lights
        bool enablePointLight = true;
        shader.setBool("enablePointLight", enablePointLight);
        if (enablePointLight) {
            const int maxPointLightShaderSupport = 1;
            int iter = 0;
            auto poinLightView = reg.view<cl::Trf, cr::PointLight>();
            for (auto ent : poinLightView) {
                if (iter >= maxPointLightShaderSupport) {
                    break;
                }

                const auto& pointLightRndCmp = poinLightView.get<cr::PointLight>(ent);
                const auto& pointLightTrfCmp = poinLightView.get<cl::Trf>(ent);

                shader.setVec3f(Sprintf("pointLights[%d].position", iter), pointLightTrfCmp.transform.position);
                shader.setVec3f(Sprintf("pointLights[%d].ambient", iter), pointLightRndCmp.setup.ambient);
                shader.setVec3f(Sprintf("pointLights[%d].diffuse", iter), pointLightRndCmp.setup.diffuse);
                shader.setVec3f(Sprintf("pointLights[%d].specular", iter), pointLightRndCmp.setup.specular);
                shader.setFloat(Sprintf("pointLights[%d].constant", iter), pointLightRndCmp.setup.constant);
                shader.setFloat(Sprintf("pointLights[%d].linear", iter), pointLightRndCmp.setup.linear);
                shader.setFloat(Sprintf("pointLights[%d].quadratic", iter), pointLightRndCmp.setup.quadratic);
                iter++;
            }
        }

        // spotLight
        bool enableSpotLight = true;
        shader.setBool("enableSpotLight", enableSpotLight);
        if (enableSpotLight) {
            const int maxSpotLightShaderSupport = 1;
            int iter = 0;
            auto spotLightView = reg.view<cl::Trf, cr::SpotLight>();
            for (auto ent : spotLightView) {
                if (iter >= maxSpotLightShaderSupport) {
                    break;
                }

                const auto& spotLightRndCmp = spotLightView.get<cr::SpotLight>(ent);
                const auto& spotLightTrfCmp = spotLightView.get<cl::Trf>(ent);

                shader.setVec3f(Sprintf("spotLight[%d].position", iter), spotLightTrfCmp.transform.position);
                shader.setVec3f(Sprintf("spotLight[%d].direction", iter), spotLightTrfCmp.transform.getForward());
                shader.setVec3f(Sprintf("spotLight[%d].ambient", iter), spotLightRndCmp.setup.ambient);
                shader.setVec3f(Sprintf("spotLight[%d].diffuse", iter), spotLightRndCmp.setup.diffuse);
                shader.setVec3f(Sprintf("spotLight[%d].specular", iter), spotLightRndCmp.setup.specular);
                shader.setFloat(Sprintf("spotLight[%d].constant", iter), spotLightRndCmp.setup.constant);
                shader.setFloat(Sprintf("spotLight[%d].linear", iter), spotLightRndCmp.setup.linear);
                shader.setFloat(Sprintf("spotLight[%d].quadratic", iter), spotLightRndCmp.setup.quadratic);
                shader.setFloat(Sprintf("spotLight[%d].cutOff", iter), spotLightRndCmp.setup.cutOff);
                shader.setFloat(Sprintf("spotLight[%d].outerCutOff", iter), spotLightRndCmp.setup.outerCutOff);
                iter++;
            }
        }

        shader.setVec3f("viewPos", camera.getPos());
        shader.setMat4f("view", cameraView);
        shader.setMat4f("projection", cameraProjection);

        Mat4f model = glm::identity<Mat4f>();
        trfCmp.transform.getModelMatrix(model);
        rndCmp.obj.shader.setMat4f("model", model);

        glBindVertexArray(rndCmp.obj.vao);

        glDrawArrays(GL_TRIANGLES, 0, rndCmp.obj.verticesSize);
    }
}

static void renderLightTexture(entt::registry& reg, const ICamera& camera, const Mat4f& cameraView, const Mat4f& cameraProjection)
{
    auto regView = reg.view<cl::Trf, cr::LightTexture>();
    for (auto ent : regView) {
        const auto& rndCmp = regView.get<cr::LightTexture>(ent);
        const auto& trfCmp = regView.get<cl::Trf>(ent);
        const auto& shader = rndCmp.obj.shader;

        ASSERT_MSG(rndCmp.obj.inited, "invalid primitive render object");

        glUseProgram(shader.handle());

        shader.setInt("material.diffuse", 0);
        shader.setInt("material.specular", 1);
        shader.setFloat("material.shininess", rndCmp.shiness);

        // directional light
        bool enableDirectLight = true;
        shader.setBool("enableDirectLight", enableDirectLight);
        if (enableDirectLight) {
            const int maxDirectLightShaderSupport = 1;
            int iter = 0;
            auto poinLightView = reg.view<cl::Trf, cr::DirectLight>();
            for (auto ent : poinLightView) {
                if (iter >= maxDirectLightShaderSupport) {
                    break;
                }

                const auto& directLightRndCmp = poinLightView.get<cr::DirectLight>(ent);
                const auto& directLightTrfCmp = poinLightView.get<cl::Trf>(ent);

                shader.setVec3f(Sprintf("dirLight[%d].direction", iter), directLightTrfCmp.transform.getForward());
                shader.setVec3f(Sprintf("dirLight[%d].ambient", iter), directLightRndCmp.setup.ambient);
                shader.setVec3f(Sprintf("dirLight[%d].diffuse", iter), directLightRndCmp.setup.diffuse);
                shader.setVec3f(Sprintf("dirLight[%d].specular", iter), directLightRndCmp.setup.specular);
                iter++;
            }
        }

        // point lights
        bool enablePointLight = true;
        shader.setBool("enablePointLight", enablePointLight);
        if (enablePointLight) {
            const int maxPointLightShaderSupport = 1;
            int iter = 0;
            auto poinLightView = reg.view<cl::Trf, cr::PointLight>();
            for (auto ent : poinLightView) {
                if (iter >= maxPointLightShaderSupport) {
                    break;
                }

                const auto& pointLightRndCmp = poinLightView.get<cr::PointLight>(ent);
                const auto& pointLightTrfCmp = poinLightView.get<cl::Trf>(ent);

                shader.setVec3f(Sprintf("pointLights[%d].position", iter), pointLightTrfCmp.transform.position);
                shader.setVec3f(Sprintf("pointLights[%d].ambient", iter), pointLightRndCmp.setup.ambient);
                shader.setVec3f(Sprintf("pointLights[%d].diffuse", iter), pointLightRndCmp.setup.diffuse);
                shader.setVec3f(Sprintf("pointLights[%d].specular", iter), pointLightRndCmp.setup.specular);
                shader.setFloat(Sprintf("pointLights[%d].constant", iter), pointLightRndCmp.setup.constant);
                shader.setFloat(Sprintf("pointLights[%d].linear", iter), pointLightRndCmp.setup.linear);
                shader.setFloat(Sprintf("pointLights[%d].quadratic", iter), pointLightRndCmp.setup.quadratic);
                iter++;
            }
        }

        // spotLight
        bool enableSpotLight = true;
        shader.setBool("enableSpotLight", enableSpotLight);
        if (enableSpotLight) {
            const int maxSpotLightShaderSupport = 1;
            int iter = 0;
            auto spotLightView = reg.view<cl::Trf, cr::SpotLight>();
            for (auto ent : spotLightView) {
                if (iter >= maxSpotLightShaderSupport) {
                    break;
                }

                const auto& spotLightRndCmp = spotLightView.get<cr::SpotLight>(ent);
                const auto& spotLightTrfCmp = spotLightView.get<cl::Trf>(ent);

                shader.setVec3f(Sprintf("spotLight[%d].position", iter), spotLightTrfCmp.transform.position);
                shader.setVec3f(Sprintf("spotLight[%d].direction", iter), spotLightTrfCmp.transform.getForward());
                shader.setVec3f(Sprintf("spotLight[%d].ambient", iter), spotLightRndCmp.setup.ambient);
                shader.setVec3f(Sprintf("spotLight[%d].diffuse", iter), spotLightRndCmp.setup.diffuse);
                shader.setVec3f(Sprintf("spotLight[%d].specular", iter), spotLightRndCmp.setup.specular);
                shader.setFloat(Sprintf("spotLight[%d].constant", iter), spotLightRndCmp.setup.constant);
                shader.setFloat(Sprintf("spotLight[%d].linear", iter), spotLightRndCmp.setup.linear);
                shader.setFloat(Sprintf("spotLight[%d].quadratic", iter), spotLightRndCmp.setup.quadratic);
                shader.setFloat(Sprintf("spotLight[%d].cutOff", iter), spotLightRndCmp.setup.cutOff);
                shader.setFloat(Sprintf("spotLight[%d].outerCutOff", iter), spotLightRndCmp.setup.outerCutOff);
                iter++;
            }
        }

        shader.setVec3f("viewPos", camera.getPos());
        shader.setMat4f("view", cameraView);
        shader.setMat4f("projection", cameraProjection);

        Mat4f model = glm::identity<Mat4f>();
        trfCmp.transform.getModelMatrix(model);
        rndCmp.obj.shader.setMat4f("model", model);

        // bind diffuse map
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, rndCmp.obj.diffuseTexture);
        // bind specular map
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, rndCmp.obj.specularTexture);

        glBindVertexArray(rndCmp.obj.vao);

        glDrawArrays(GL_TRIANGLES, 0, rndCmp.obj.verticesSize);
    }
}

void CubeRenderSys::update(entt::registry& reg, const ICamera& camera)
{
#if defined(DEBUG_RENDER)
    extern void DebugRender(const ICamera& camera);
    DebugRender(camera);
    return;
#endif // #ifdef DEBUG_RENDER

    Mat4f view = glm::identity<Mat4f>();
    Mat4f projection = glm::identity<Mat4f>();
    camera.getViews(view, projection);

    renderPrimSolid(reg, view, projection);
    renderPrimTexture(reg, view, projection);
    renderDirectLight(reg, view, projection);
    renderPointLight(reg, view, projection);
    renderSpotLight(reg, view, projection);
    renderLightMaterial(reg, camera, view, projection); // wrong work
    renderLightTexture(reg, camera, view, projection);
}

#if defined(DEBUG_RENDER)

struct Vertex {
    Vec3f Position;
    Vec3f Normal;
    Vec2f TexCoords;
};

struct Texture {
    uint id;
    String type;
    String path;
};

class Mesh {
public:
    /*  Mesh Data  */
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;
    /*  Functions  */
    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures);
    void Draw(ShaderRef shader);

private:
    /*  Render data  */
    uint VAO, VBO, EBO;
    /*  Functions    */
    void setupMesh();
};

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures)
{
    this->vertices = vertices;
    this->indices = indices;
    this->textures = textures;

    setupMesh();
}

void Mesh::setupMesh()
{
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int),
        &indices[0], GL_STATIC_DRAW);

    // vertex positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    // vertex normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
    // vertex texture coords
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));

    glBindVertexArray(0);
}

#include <sstream>

/*void Mesh::Draw(ShaderRef shader)
{
    unsigned int diffuseNr = 1;
    unsigned int specularNr = 1;
    for (unsigned int i = 0; i < textures.size(); i++) {
        glActiveTexture(GL_TEXTURE0 + i); // активируем текстурный блок, до привязки
        // получаем номер текстуры
        std::stringstream ss;
        std::string number;
        std::string name = textures[i].type;
        if (name == "texture_diffuse")
            ss << diffuseNr++; // передаем unsigned int в stream
        else if (name == "texture_specular")
            ss << specularNr++; // передаем unsigned int в stream
        number = ss.str();

        shader.setFloat(("material." + name + number).c_str(), i);
        glBindTexture(GL_TEXTURE_2D, textures[i].id);
    }
    glActiveTexture(GL_TEXTURE0);

    // отрисовывем полигональную сетку
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}*/

void Mesh::Draw(ShaderRef shader)
{
    // bind appropriate textures
    unsigned int diffuseNr = 1;
    unsigned int specularNr = 1;
    unsigned int normalNr = 1;
    unsigned int heightNr = 1;
    for (unsigned int i = 0; i < textures.size(); i++) {
        glActiveTexture(GL_TEXTURE0 + i); // active proper texture unit before binding
        // retrieve texture number (the N in diffuse_textureN)
        String number;
        String name = textures[i].type;
        if (name == "texture_diffuse") {
            number = std::to_string(diffuseNr++);
        } else if (name == "texture_specular") {
            continue;
            number = std::to_string(specularNr++); // transfer unsigned int to stream
        } else if (name == "texture_normal") {
            continue;
            number = std::to_string(normalNr++); // transfer unsigned int to stream
        } else if (name == "texture_height") {
            continue;
            number = std::to_string(heightNr++); // transfer unsigned int to stream
        }

        // now set the sampler to the correct texture unit
        shader.setInt(String(name + number), i);
        //glUniform1i(glGetUniformLocation(shader.ID, (name + number).c_str()), i);
        // and finally bind the texture
        glBindTexture(GL_TEXTURE_2D, textures[i].id);
    }

    // draw mesh
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    // always good practice to set everything back to defaults once configured.
    glActiveTexture(GL_TEXTURE0);
}

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

class Model {
public:
    /*  Методы   */
    Model(char* path)
    {
        loadModel(path);
    }
    void Draw(ShaderRef shader);

private:
    /*  Данные модели  */
    std::vector<Mesh> meshes;
    std::vector<Texture> textures_loaded;
    std::string directory;
    /*  Методы   */
    void loadModel(StringRef path);
    void processNode(aiNode* node, const aiScene* scene);
    Mesh processMesh(aiMesh* mesh, const aiScene* scene);
    std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, StringRef typeName);
};

void Model::Draw(ShaderRef shader)
{
    for (unsigned int i = 0; i < meshes.size(); i++)
        meshes[i].Draw(shader);
}

void Model::loadModel(StringRef path)
{
    Assimp::Importer import;
    const aiScene* scene = import.ReadFile(path.data(), aiProcess_Triangulate | aiProcess_FlipUVs);

    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
        ASSERT_FAIL("ERROR::ASSIMP::%s", import.GetErrorString());
        return;
    }
    directory = path.substr(0, path.find_last_of('/'));

    processNode(scene->mRootNode, scene);
}

void Model::processNode(aiNode* node, const aiScene* scene)
{
    // обработать все полигональные сетки в узле(если есть)
    for (unsigned int i = 0; i < node->mNumMeshes; i++) {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        meshes.push_back(processMesh(mesh, scene));
    }
    // выполнить ту же обработку и для каждого потомка узла
    for (unsigned int i = 0; i < node->mNumChildren; i++) {
        processNode(node->mChildren[i], scene);
    }
}

Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene)
{
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;

    for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
        Vertex vertex;
        // обработка координат, нормалей и текстурных координат вершин
        Vec3f vector;
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.Position = vector;
        vertices.push_back(vertex);

        vector.x = mesh->mNormals[i].x;
        vector.y = mesh->mNormals[i].y;
        vector.z = mesh->mNormals[i].z;
        vertex.Normal = vector;

        if (mesh->mTextureCoords[0]) // сетка обладает набором текстурных координат?
        {
            glm::vec2 vec;
            vec.x = mesh->mTextureCoords[0][i].x;
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.TexCoords = vec;
        } else {
            vertex.TexCoords = glm::vec2(0.0f, 0.0f);
        }
    }
    // орбаботка индексов
    for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
        aiFace face = mesh->mFaces[i];
        for (unsigned int j = 0; j < face.mNumIndices; j++)
            indices.push_back(face.mIndices[j]);
    }
    // обработка материала
    if (mesh->mMaterialIndex >= 0) {
        aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
        std::vector<Texture> diffuseMaps = loadMaterialTextures(material,
            aiTextureType_DIFFUSE, "texture_diffuse");
        textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
        std::vector<Texture> specularMaps = loadMaterialTextures(material,
            aiTextureType_SPECULAR, "texture_specular");
        textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
    }

    return Mesh(vertices, indices, textures);
}

#include <SDL_image.h>

struct TextureRaw2 {

    TextureRaw2(StringRef texturePath)
    {
        surface = IMG_Load(texturePath.data());
        ASSERT_MSG(surface, "Can't IMG_Load(%s). Error: %s", texturePath.data(), IMG_GetError());
    }

    ~TextureRaw2()
    {
        SDL_FreeSurface(surface);
    }

    int w() { return surface->w; }
    int h() { return surface->h; }
    void* data() { return surface->pixels; }

    int mode()
    {
        if (4 == surface->format->BytesPerPixel) {
            return GL_RGBA;
        }
        ASSERT(3 == surface->format->BytesPerPixel);
        return GL_RGB;
    }

private:
    SDL_Surface* surface = nullptr;
};

static uint TextureFromFile(const char* path, const String& directory, bool gamma = false)
{
    String filename = String(path);
    filename = directory + '/' + filename;

    uint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    TextureRaw2 rawTex(filename.c_str());
    glTexImage2D(GL_TEXTURE_2D, 0, rawTex.mode(), rawTex.w(), rawTex.h(), 0, rawTex.mode(), GL_UNSIGNED_BYTE, rawTex.data());
    glGenerateMipmap(GL_TEXTURE_2D);

    return textureID;
}

std::vector<Texture> Model::loadMaterialTextures(aiMaterial* mat, aiTextureType type, StringRef typeName)
{
    std::vector<Texture> textures;
    for (unsigned int i = 0; i < mat->GetTextureCount(type); i++) {
        aiString str;
        mat->GetTexture(type, i, &str);
        bool skip = false;
        for (unsigned int j = 0; j < textures_loaded.size(); j++) {
            if (textures_loaded[j].path == String(str.C_Str())) {
                textures.push_back(textures_loaded[j]);
                skip = true;
                break;
            }
        }
        if (!skip) { // если текстура не была загружена – сделаем это
            Texture texture;
            texture.id = TextureFromFile(str.C_Str(), directory);
            texture.type = typeName;
            texture.path = str.C_Str();
            textures.push_back(texture);
            // занесем текстуру в список уже загруженных
            textures_loaded.push_back(texture);
        }
    }
    return textures;
}

static Shader modelShader;

static void DebugRenderInit()
{
    static bool isInited = false;
    if (isInited) {
        return;
    }
    isInited = true;

    modelShader.compile("shaders/model_debug/model_debug.vsh", "shaders/model_debug/model_debug.fsh");
}

void DebugRender(const ICamera& camera)
{
    DebugRenderInit();

    static Model model3d("backpack/backpack.obj");

    ShaderRef shader(modelShader);

    glUseProgram(shader.handle());

    Mat4f view = glm::identity<Mat4f>();
    Mat4f projection = glm::identity<Mat4f>();
    camera.getViews(view, projection);

    shader.setMat4f("projection", projection);
    shader.setMat4f("view", view);

    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(0.0f, -1.75f, 0.0f)); // translate it down so it's at the center of the scene
    model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f)); // it's a bit too big for our scene, so scale it down
    shader.setMat4f("model", model);

    model3d.Draw(shader);
}
#endif // #if defined(DEBUG_RENDER)
