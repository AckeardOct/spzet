#pragma once

#include "i_camera.h"

class Camera3D : public ICamera {
public:
    Camera3D(GameWindow& window, Vec3f pos = { 0.f, 0.f, 0.f }, Vec3f up = { 0.f, 1.f, 0.f });

public: // ICamera interface
    virtual void getViews(Mat4f& view, Mat4f& projection) const override;
    virtual Vec3f getPos() const override { return position; }
    virtual Vec3f getFront() const { return front; }
    virtual void setPos(const Vec3f& vec) override { position = vec; }

public:
    void dbgIncYaw(float val) { yaw += val; }
    void dbgIncPitch(float val) { pitch += val; }
    void dbgUpdateCameraVectors() { updateCameraVectors(); }

    void dbgProcessMovement(const Vec3f& dir)
    {
        position += front * dir.z;
        position += right * dir.x;
        position += up * dir.y;
    }

    void dbgProcessMouse(const Vec2f& mouseShift, bool constraintPitch = true)
    {
        yaw += mouseShift.x * mouseSensitivity;
        pitch += -1 * mouseShift.y * mouseSensitivity;

        if (constraintPitch) {
            if (pitch > 89.0f)
                pitch = 89.0f;
            if (pitch < -89.0f)
                pitch = -89.0f;
        }
        updateCameraVectors();
    }

private:
    void updateCameraVectors();

private:
    Vec3f position;
    Vec3f front;
    Vec3f up;
    Vec3f right;
    Vec3f worldUp;

    // Euler Angles
    float yaw = 0.f;
    float pitch = 0.f;

    // Camera options
    float movementSpeed = 0.f;
    float mouseSensitivity = 0.f;
    float zoom = 0.f;
};
