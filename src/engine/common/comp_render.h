#pragma once

#include <vector>

#include "engine/render/color.h"
#include "engine/render/probj_storage.h"

namespace cr {

struct PrimSolid {
    PrimSolid(StringRef templateName, Color color = Color_Black)
        : color(color)
    {
        obj = probjStorage.get(templateName);
    }

    Probj obj;
    Color color;
};

struct PrimTexture {
    PrimTexture(StringRef templateName)
    {
        obj = probjStorage.get(templateName);
    }

    Probj obj;
};

struct DirectLight {
    struct Setup {
        Color ambient;
        Color diffuse;
        Color specular;
    };

public:
    DirectLight(StringRef templateName, const Setup& setup)
        : setup(setup)
    {
        obj = probjStorage.get(templateName, "direct_light");
    }

    Probj obj;
    Setup setup;
};

struct SpotLight {
    struct Setup {
        Color ambient;
        Color diffuse;
        Color specular;

        float constant = 0.f;
        float linear = 0.f;
        float quadratic = 0.f;

        float cutOff = 0.f;
        float outerCutOff = 0.f;
    };

public:
    SpotLight(StringRef templateName, const Setup& setup)
        : setup(setup)
    {
        obj = probjStorage.get(templateName, "spot_light");
    }

    Probj obj;
    Setup setup;
};

struct PointLight {
    struct Setup {
        Color ambient;
        Color diffuse;
        Color specular;

        float constant = 0.f;
        float linear = 0.f;
        float quadratic = 0.f;
    };

public:
    PointLight(StringRef templateName, const Setup& setup)
        : setup(setup)
    {
        obj = probjStorage.get(templateName, "point_light");
    }

    Probj obj;
    Setup setup;
};

struct LightTexture {
    LightTexture(StringRef templateName, float shiness)
        : shiness(shiness)
    {
        obj = probjStorage.get(templateName);
    }

    Probj obj;
    float shiness = 0.f;
};

// wrong work
struct MaterialTexture {
    struct Setup {
        Color ambient;
        Color diffuse;
        Color specular;

        float shiness = 0.f;
    };

    MaterialTexture(StringRef templateName, const Setup& setup)
        : setup(setup)
    {
        obj = probjStorage.get(templateName);
    }

    Probj obj;
    Setup setup;
};

}
