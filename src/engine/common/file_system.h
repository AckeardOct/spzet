#pragma once

#include "engine/common/str.h"

namespace fs {

bool exists(StringRef filePath);

String cat(StringRef filePath);

} // namespace fs
