#pragma once

#include "engine/common/comp_logic.h"
#include "engine/common/i_system.h"

#include <SDL2/SDL_events.h>
#include <unordered_map>
#include <vector>

struct InputSys : public ILogicSys {
    InputSys();

    void onInput(const SDL_Event& event);

public: // ILogicSys interface
    virtual void update(entt::registry& reg, float dt) override;

private:
    std::vector<SDL_Event> events;
    std::unordered_map<int, bool> keyboardKeys;
    std::unordered_map<int, bool> mouseKeys;
    Vec2f mousePos = Vec2f_Zero;
    Vec2f mouseShift = Vec2f_Zero;
    bool firstMouseEvent = true;
};

struct MoveSys : public ILogicSys {
public: // ILogicSys interface
    virtual void update(entt::registry& reg, float dt) override {}
};
