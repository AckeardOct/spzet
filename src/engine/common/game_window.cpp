#include "game_window.h"

#include <GLES3/gl3.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "i_scene.h"
#include "logger.h"

const char* TITLE = "View";
const Vec2f WINDOW_SIZE(640, 480);
const float FPS = 60.f;

GameWindow::GameWindow(int argc, char** argv)
{
    initSDL();
    initOpenGL();
    scene = IScene::Make("CubeScene", *this);
}

GameWindow::~GameWindow()
{
    delete scene;
    scene = nullptr;

    SDL_GL_DeleteContext(gl_context);
    gl_context = nullptr;

    SDL_DestroyRenderer(sdl_renderer);
    sdl_renderer = nullptr;

    SDL_DestroyWindow(sdl_window);
    sdl_window = nullptr;

    SDL_Quit();
}

void GameWindow::loop()
{
    const float frameLength_ms = 1000.f / FPS;
    float dt = 0;
    uint32_t time_ms = SDL_GetTicks();
    while (!quitRequested) {
        { // check fps
            uint32_t newTime_ms = SDL_GetTicks();
            dt = newTime_ms - time_ms;
            if (dt < frameLength_ms) {
                uint32_t diff = frameLength_ms - dt;
                if (diff > 5) {
                    SDL_Delay(diff / 2);
                }
                continue;
            }
            time_ms = newTime_ms;
        }

        processInput(dt);
        update(dt);
        render(dt);
    }
}

Vec2f GameWindow::getSize() const
{
    return WINDOW_SIZE;
}

Vec2f GameWindow::getCenter() const
{
    return WINDOW_SIZE / 2.f;
}

bool GameWindow::initSDL()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        ASSERT_FAIL("SDL_Init(SDL_INIT_VIDEO) failed. error: %s", SDL_GetError());
        return false;
    }

    int imgMask = IMG_INIT_JPG | IMG_INIT_PNG;
    if (!(IMG_Init(imgMask) & (imgMask))) {
        ASSERT_FAIL("IMG_Init() failed. error: %s", IMG_GetError());
        return false;
    }

    sdl_window = SDL_CreateWindow(TITLE,
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        WINDOW_SIZE.x,
        WINDOW_SIZE.y,
        SDL_WINDOW_SHOWN);
    if (sdl_window == nullptr) {
        ASSERT_FAIL("SDL_CreateWindow() failed. error: %s", SDL_GetError());
        return false;
    }

    sdl_renderer = SDL_CreateRenderer(sdl_window, -1, SDL_RENDERER_ACCELERATED);
    if (sdl_renderer == nullptr) {
        ASSERT_FAIL("SDL_CreateRenderer() failed. error: %s", SDL_GetError());
        return false;
    }

    return true;
}

bool GameWindow::initOpenGL()
{
    gl_context = SDL_GL_CreateContext(sdl_window);
    if (gl_context == nullptr) {
        ASSERT_FAIL("Can't create OpenGlContext");
        return false;
    }

    int res = 0;
    res = SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    ASSERT(0 == res);

    res = SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    ASSERT(0 == res);

    res = SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    ASSERT(0 == res);

    res = SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    ASSERT(0 == res);

    res = SDL_GL_SetSwapInterval(1);
    ASSERT(0 == res);

    glEnable(GL_DEPTH_TEST);

    return true;
}

void GameWindow::processInput(float dt)
{
    SDL_Event sdl_event;
    while (SDL_PollEvent(&sdl_event)) {
        if (scene) {
            scene->input(dt, sdl_event);
        }

        switch (sdl_event.type) {
        case SDL_QUIT:
            quitRequested = true;
            break;
        }
    }
}

void GameWindow::update(float dt)
{
    if (scene) {
        scene->update(dt);
    }
}

void GameWindow::render(float dt)
{
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (scene) {
        scene->render(dt);
    }

    SDL_GL_SwapWindow(sdl_window);
}
