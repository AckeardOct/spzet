#pragma once

#include "engine/common/i_camera.h"
#include "engine/math/transform.h"

namespace cl {

struct Trf {
    Transform transform;
};

struct CameraInput {
    CameraInput(ICamera* camera, float speed)
        : camera(camera)
        , speed(speed)
    {
    }

    ICamera* camera;
    float speed;
};

}
