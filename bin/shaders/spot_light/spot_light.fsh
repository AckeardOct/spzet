#version 300 es
precision mediump float;

out vec4 fragColor;

uniform vec3 diffuseColor;

void main()
{
    fragColor = vec4(diffuseColor, 0);
}
