#version 300 es
precision mediump float;

in vec2 Texel;

out vec4 fragColor;

uniform sampler2D gSampler;

void main()
{
    fragColor = texture2D(gSampler, Texel.xy);
}
