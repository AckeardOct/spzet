#!/usr/bin/env python3

def ABS_PATH(path):
    from os.path import abspath
    return abspath(path)


def JOIN_PATH(left, *right):
    from os.path import join
    return join(left, *right)


def EXISTS(path):
    from os.path import exists
    return exists(path)


def MK_DIRS(path):
    from os import makedirs
    return makedirs(path)


def RM_DIRS(path):
    from shutil import rmtree
    if EXISTS(path):
        rmtree(path)


def RM_FILE(path):
    from os import remove
    if EXISTS(path):
        remove(path)


def EXECUTE(cmd):
    from os import system
    print('========== EXECUTE COMMAND: ')
    print(cmd)
    print('====================')
    return system(cmd)

def RM_FILES_RECUR(path, *masks):
    from glob import glob
    for mask in masks:
        files = glob(path + '/**/' + mask, recursive=False)
        for file in files:
            RM_FILE(file)

def FIND_FILES(path, *masks, recur = False):
    from glob import glob
    files = []
    for mask in masks:
        files += glob(path + '/**/' + mask, recursive=recur)
    return files

# ========== SCRIPT ========== #


build_dir = ABS_PATH("./build")


def welcome():
    print("Now we install all dependencies for your project! \n")
    if not EXISTS(build_dir):
        MK_DIRS(build_dir)


def conan():
    print("\n\nConan install")
    print("========================")
    cmd = "conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan"
    EXECUTE(cmd)

    conan_dir = JOIN_PATH(build_dir, 'conan')
    RM_DIRS(conan_dir)
    MK_DIRS(conan_dir)
    cmd = str.format("conan install ./conanfile.txt -if {} --build=missing", conan_dir)
    EXECUTE(cmd)
    print("========== Conan installed ==========")


def main():
    welcome()
    conan()

if __name__ == "__main__":
    main()